class_name Player
extends CharacterBody3D

const SPEED = 1.5
const JUMP_VELOCITY = 4.5
const LOOK_OVERRIDE_TIMEOUT = 2.5

@onready var camera: Camera = $"../Camera"

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var look_input_timeout = LOOK_OVERRIDE_TIMEOUT
var current_interp_area: InterpArea = null

func _physics_process(delta):
	# Add the gravity.
	if not is_on_floor():
		velocity.y -= gravity * delta

	var input_dir = Input.get_vector("move_left", "move_right", "move_fwd", "move_bwd")
	var direction = camera.map_input(input_dir).normalized()
	if direction:
		velocity.x = direction.x * SPEED
		velocity.z = direction.z * SPEED
		
		$ArrowRed.global_rotation.y = Vector2(direction.x, direction.z).angle_to(Vector2(1,0))
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		velocity.z = move_toward(velocity.z, 0, SPEED)
	
	
	var look_dir = Input.get_vector("look_left", "look_right", "look_fwd", "look_bwd")
	var look_direction = camera.map_input(look_dir).normalized()
	if look_direction:
		look_input_timeout = 0
		$ArrowGreen.global_rotation.y = Vector2(look_direction.x, look_direction.z).angle_to(Vector2(1,0))
	else:
		look_input_timeout += delta
		
		if look_input_timeout > LOOK_OVERRIDE_TIMEOUT and direction:
			$ArrowGreen.global_rotation.y = Vector2(direction.x, direction.z).angle_to(Vector2(1,0))
	
	# Clamp look angle
	var body_angle = $ArrowRed.global_rotation.y
	var agl = $ArrowGreen.global_rotation.y - body_angle
	if agl > PI: agl -= 2*PI
	$ArrowGreen.global_rotation.y = clamp(agl, -0.7*PI, 0.7*PI) + body_angle
	
	move_and_slide()
	
	if current_interp_area != null:
		current_interp_area.interp(self, camera)
