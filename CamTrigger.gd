extends Area3D

@export_group("Zoom and fov")
@export var apply_zoom_fov = true
@export var cam_distance = 38.0
@export var cam_fov = 10.0

@export_group("Rotation")
@export var apply_rotation = true
@export var yaw_degrees = 0.0
@export var pitch_degreees = 15.0


@onready var camera: Camera = $"../Camera"

func _ready():
	self.connect("body_entered", on_body_enter)


func on_body_enter(body: Node3D):
	if not body is Player:
		return
	
	if apply_rotation:
		camera.tween_rotation(deg_to_rad(yaw_degrees), deg_to_rad(pitch_degreees))
		
	if apply_zoom_fov:
		camera.tween_fov(cam_distance, cam_fov)
