class_name Camera
extends Node3D

@onready var follow_node = $"../Player"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func _physics_process(delta):
	global_position = follow_node.global_position


func map_input(i: Vector2) -> Vector3:
	var v = i.rotated(-$Yaw.rotation.y)
	return Vector3(v.x, 0, v.y)


func adjust_yaw(value: float):
	$Yaw.rotation.y = value

func adjust_pitch(value: float):
	$Yaw/Pitch.rotation.x = value

func adjust_dist(value: float):
	$Yaw/Pitch/Camera3D.position.y = value

func adjust_fov(value: float):
	$Yaw/Pitch/Camera3D.fov = value

func tween_rotation(yaw, pitch):
	get_tree().create_tween().tween_property($Yaw, "rotation:y", yaw, 1.5)
	get_tree().create_tween().tween_property($Yaw/Pitch, "rotation:x", pitch, 1.5)


func tween_fov(distance, fov):
	get_tree().create_tween().tween_property($Yaw/Pitch/Camera3D, "position:y", distance, 1.5)
	get_tree().create_tween().tween_property($Yaw/Pitch/Camera3D, "fov", fov, 1.5)
